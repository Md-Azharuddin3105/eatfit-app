import React from 'react';
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css';
import ItemDetail from './Components/ItemDetail'
import Header from './Components/Header'
import Showcase from './Components/Showcase'
import Footer from './Components/Footer'
import CartPage from './Components/CartPage'
import backgroundImage1 from './Components/Images/1st_backgroundImage.jpg'
import backgroundImage2 from './Components/Images/2nd_backgroundImage.jpg'
import backgroundImage3 from './Components/Images/3rd_backgroundImage.jpg'
import backgroundImage4 from './Components/Images/4th_backgroundImage.jpg'
import store from './Redux/Store'
function App() {
  return (
    <Provider store = {store}>
      <Router>
          <div className="App">
              <Route path = '/' exact>
                <Header/>
                  <div className='backgroundImages'>
                    <img className='landingImages' src={backgroundImage1} height='auto' width='280px' alt=''/>
                    <img className='landingImages' src={backgroundImage2} height='auto' width='280px' alt=''/>
                    <img className='landingImages' src={backgroundImage3} height='auto' width='280px' alt=''/>
                    <img className='landingImages' src={backgroundImage4} height='auto' width='280px' alt=''/>
                  </div>
                <Showcase/>
                <Footer/>
              </Route>
              <Route path = '/cart'>
                <CartPage/>
              </Route>
              <ItemDetail/>
          </div>
      </Router>
    </Provider>
  );
}
export default App;