import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { incrementCartValue, decrementCartValue, clearCart} from '../Redux/Action'
import Footer from './Footer'
// import HeaderCart from './HeaderCart'
import Header from './Header'
import emptyCartImage from './Images/empty_cart.png'
import './CartPage.css'

class CartPage extends Component {
    componentDidMount(){
        console.log(this.props)
    }
    placeOrder = () => {
        this.props.dispatch(clearCart())
        alert('Order placed successfully')
    }
    render() {
        return (
            <div className='cart'>
                <Header cartValue={this.props.state.cartValue}/>
                <div className='cartPage'>
                <Link className='goBack' to = '/'>Back To Home</Link>
                    {this.props.state.cartValue ? 
                    this.props.state.cartData.map((item) => {
                        return (
                            <div className='cartItem'>
                                <div className='cartImage'>
                                    <Link to = {`/${item.id}`}><img src = {item.image} alt  = '' height='auto' width='200px'/></Link>
                                </div>
                                <div className='cartItemDetail'>
                                    <h3>{item.name}</h3>
                                    <h3>${item.price}</h3>
                                    <button className='changeQuantity' onClick = { () => this.props.dispatch(incrementCartValue(item))}>+</button>&nbsp;&nbsp;
                                    <span>{this.props.state.quantityOfEachItem[item.id]}</span>&nbsp;&nbsp;
                                    <button className='changeQuantity' onClick = { () => this.props.dispatch(decrementCartValue(item))}>-</button>
                                </div>
                            </div>
                        )
                    })
                    : 
                    <div>
                        <h3 className='emptyCart'>Your Cart is empty</h3>
                        <img className='emptyCartLogo' src = {emptyCartImage} alt = '' height='auto' width='250px' />
                    </div>}
                    {this.props.state.cartValue ? 
                    <div>
                        <div className='totalAmount'><h3>Total Amount:- ${this.props.state.totalAmount}</h3></div>
                        <Link to = '/'><button className='orderDetail' onClick ={this.placeOrder}><h3>Place Order</h3></button></Link>
                    </div> :
                    <div></div>}
                </div>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        state
    }
}
const mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
} 
export default connect (mapStateToProps, mapDispatchToProps)(CartPage)