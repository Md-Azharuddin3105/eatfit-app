import React, { Component } from 'react'
import WhiteLogo from './Images/whitelogo.png'
import appStoreLogo from './Images/app-store.png'
import playStoreLogo from './Images/play-store.png'
import './Footer.css'

export class Footer extends Component {
    render() {
        return (
            <div className='footer'>
                <div className='firstPart'>
                    <img className='logo' src={WhiteLogo} alt='' height='auto' width='120px'/>
                    <div className='text'>At eat.fit, we make group workouts fun, daily food healthy<br></br>medical & lifestyle care hassle-free. #BeBetterEveryDay</div>
                </div>
                <div className='secondPart'>
                    <div className='firstSec'>
                        <div>CONTACT US</div>
                        <div>BLOG</div>
                        <div>TALKS</div>
                        <div>CAREERS</div>
                    </div>
                    <div className='secondSec'>
                        <img src={appStoreLogo} alt='' height='auto' width='180'/>
                        <img src={playStoreLogo} alt='' height='auto' width='180'/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer
