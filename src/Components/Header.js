import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import logo from './Images/logo.png'
import cart_icon from './Images/cart.png'
import './Header.css'
export class Header extends Component {
    render() {
        return (
            <div className='header'>
                <Link to = '/'><img src={logo} height='auto' width='180px' alt='' className='logo'/></Link>
                <div className='end'>
                    <div className='cartQuantity'>{this.props.cartValue}</div>
                    <Link to = '/cart'><img src={cart_icon} alt='' className='cartIcon' height='auto' width='50px'/></Link>
                    <Link to = '/cart' className='cart'><h2>Cart</h2></Link>
                    <h2>Login</h2>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return{
        cartValue: state.cartValue
    }
}

export default connect(mapStateToProps)(Header)