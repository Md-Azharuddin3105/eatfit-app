import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import logo from './Images/logo.png'
import cart_icon from './Images/cart.png'
import './HeaderCart.css'
export class HeaderCart extends Component {
    render() {
        return (
            <div className='headerCart'>
                <Link to = '/'><img src={logo} height='70px' width='180px' alt='' className='logo'/></Link>
                <h2>Your Cart</h2>
                <div className='endSec'>
                    <div className='cartQuantities'>{this.props.cartValue}</div>
                    <Link to = '/cart'><img src={cart_icon} alt='' className='cartIcon' height='auto' width='50px'/></Link>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return{
        cartValue: state.cartValue
    }
}
export default HeaderCart