import React, { Component } from 'react'
import { connect } from 'react-redux'
import './ItemDetail.css'
import { Route, Link } from 'react-router-dom'
import Header from './Header'
import Footer from './Footer'
import { addItemToCart } from '../Redux/Action'

class ItemDetail extends Component {
    render() {
        return (
            <div>
                {this.props.data.map((item) => {
                    return(
                        <Route path={`/${item.id}`}>
                            <div className='itemDetailContainer'>
                                <Header/>
                                <div className='itemDetailShowCase'>
                                    <div>
                                        <Link to = '/' className='backToHome'>Back To Home</Link>
                                    </div>
                                    <div className='itemDetail'>
                                        <img className='itemImage' src={item.image} alt='' height='auto' width='300px'/>
                                        <div>
                                            <h3>{item.restorent}</h3>
                                            <h3>{item.name}</h3>
                                            <h3>{item.discription}</h3>
                                            <h3>Rating:- {item.rating}</h3>
                                            <h3>Price:- ${item.price}</h3>
                                            <button onClick={ () => this.props.dispatch(addItemToCart(item))}><h3>ADD ITEM TO CART</h3></button>
                                        </div>
                                    </div>
                                </div>
                                <Footer/>
                            </div>
                        </Route>
                    )
                })}
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        data: state.data
    }
}
const mapDispatchToProps = dispatch => {
    return{
        dispatch
    }
}
export default connect (mapStateToProps,mapDispatchToProps)(ItemDetail)