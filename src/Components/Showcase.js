import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { addItemToCart } from '../Redux/Action'
import './Showcase.css'

class Showcase extends Component {
    render() {
        return (
            <div className='showcase'>
                {this.props.data.map((item) => {
                    return(
                        <div className='card' key={item.id}>
                            <Link to ={`/${item.id}`}><img className='images' src={item.image} height='auto' width='380px' alt=''/></Link>
                            <h3 className='detail'>Rating:- {item.rating}</h3>
                            <h3 className='detail'>{item.name}</h3>
                            <h3 className='detail'>Price:- ${item.price}</h3>
                            <div className='addToCart'>
                                <button className='cartButton' onClick = { () => this.props.dispatch(addItemToCart(item))}><h3>ADD TO CART</h3></button>
                            </div>
                        </div>
                    )  
                })}
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        data: state.data
    }
}
const mapDispatchToProps = dispatch => {
    return{
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Showcase)