import chickenBiryaniImage from './Components/Images/chicken_biryani.jpg'
import fruitSaladImage from './Components/Images/fruit_salad.jpg'
import paneerButterMasalaImage from './Components/Images/paneer_butter_masala.jpg'
import fishFryImage from './Components/Images/fish_fry.jpg'
import chocolateMilkShakeImage from './Components/Images/chocolate_shake.jpg'
import kadhaiMashroom from './Components/Images/kadai_mushroom.jpg'
const data = [
    {
        "id": 1,
        "name": "Chicken Biryani",
        "image": chickenBiryaniImage,
        "discription": "Best Biryani in the town, served for two",
        "restorent": "The Great Indian Restorent",
        "price": 25,
        "rating": 4
    },
    {
        "id": 2,
        "name": "Fruit Salad",
        "image": fruitSaladImage,
        "discription": "Made by the best chef of the town, served for two",
        "restorent": "The Great Indian Restorent",
        "price": 30,
        "rating": 4.5
    },
    {
        "id": 3,
        "name": "Paneer Butter Masala",
        "image": paneerButterMasalaImage,
        "discription": "Perfect Indian style Paneer Butter Masla, served for three",
        "restorent": "Arsalan",
        "price": 35,
        "rating": 5
    },
    {
        "id": 4,
        "name": "Fish Fry",
        "image": fishFryImage,
        "discription": "Bengal famous Fish Fry made with the fresh Fish",
        "restorent": "Arsalan",
        "price": 20,
        "rating": 4
    },
    {
        "id": 5,
        "name": "Chocolate Milk Shake",
        "image": chocolateMilkShakeImage,
        "discription": "Made up with quality chocolate and fresh milk",
        "restorent": "Arsalan",
        "price": 10,
        "rating": 3
    },
    {
        "id": 6,
        "name": "Kadhai Mashroom",
        "image": kadhaiMashroom,
        "discription": "Made up with fresh mashroom and best ingridients",
        "restorent": "Arsalan",
        "price": 15,
        "rating": 5
    }
]
export default data