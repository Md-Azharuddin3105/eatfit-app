export const addItemToCart = (item) => {
    return {
        type: 'addItemToCart',
        payload: item
    }
}
export const incrementCartValue = (item) => {
    return {
        type: 'incrementCartValue',
        payload: item
    }
}
export const decrementCartValue = (item) => {
    return {
        type: 'decrementCartValue',
        payload: item
    }
}
export const clearCart = () => {
    return {
        type: 'clearCart'
    }
}