import data from '../Data'
const initialState = {
    data,
    cartData: [],
    quantityOfEachItem: {"1": 0, "2": 0, "3": 0, "4": 0 ,"5": 0, "6" :0},
    cartValue: 0,
    totalAmount: 0
}
const reducer = (state = initialState, action) => {
    console.log(state, action, state.cartData.includes(action.payload))
    switch(action.type){
        case 'addItemToCart':
            let array = state.cartData
            if(!state.cartData.includes(action.payload)){
                array.push(action.payload)
            }
            let itemQuantity = state.quantityOfEachItem
            itemQuantity[action.payload.id] += 1
            return{
                ...state,
                cartData: array,
                quantityOfEachItem: itemQuantity,
                cartValue: state.cartValue + 1,
                totalAmount: state.totalAmount + action.payload.price
            }
        case 'incrementCartValue':
            let itemQuantity1 = state.quantityOfEachItem
            itemQuantity1[action.payload.id] += 1
            return{
                ...state,
                cartValue: state.cartValue + 1,
                quantityOfEachItem: itemQuantity1,
                totalAmount: state.totalAmount + action.payload.price
            }
        case 'decrementCartValue' :
            // let noOfItem = cartItem.action.payload.id
            let itemQuantity2 = state.quantityOfEachItem
            let itemInCart = state.cartData
            let isItemPresent = true
            if(itemQuantity2[action.payload.id] === 1){
                isItemPresent = false
            }
            if(!isItemPresent){
                console.log(itemInCart.indexOf(action.payload))
                itemInCart.splice(itemInCart.indexOf(action.payload), 1)
                console.log(itemInCart)
            }
            itemQuantity2[action.payload.id] -= 1
            return{
                ...state,
                cartData: itemInCart,
                cartValue: state.cartValue ? state.cartValue - 1 : state.cartValue,
                quantityOfEachItem: itemQuantity2,
                totalAmount: state.cartValue ? state.totalAmount - action.payload.price : state.totalAmount
            }
        case 'clearCart':
            return {
                data,
                cartData: [],
                quantityOfEachItem: {"1": 0, "2": 0, "3": 0, "4": 0 ,"5": 0, "6" :0},
                cartValue: 0,
                totalAmount: 0
            }
        default :
            return state
    }
}
export default reducer